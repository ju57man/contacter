package com.ju57man.contacter;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Contact_Info extends AppCompatActivity {
    String phone;
    String fullname;
    int id_contact,ava;
    String date,adress,group;
    TextView tv_tool_name,tv_tool_phone;
    ImageView avatar;
    EditText et_date, et_address, et_name, et_phone;
    FloatingActionButton save,delete;
    AlertDialog.Builder alert;
    Contact_Info x;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info_main);
        Bundle extras = getIntent().getExtras();


        phone = extras.getString("phonenumber");
        fullname = extras.getString("name");
        date = extras.getString("birth");
        adress = extras.getString("adr");
        group = extras.getString("gr");
        ava = extras.getInt("ava");
        id_contact = extras.getInt("idcon");
        x=this;

        tv_tool_name = findViewById(R.id.name_toolbar);
        tv_tool_phone = findViewById(R.id.phone_toolbar);
        et_name = findViewById(R.id.input_name);
        et_phone = findViewById(R.id.input_phone);
        et_date = findViewById(R.id.input_birthdate);
        et_address = findViewById(R.id.input_address);
        avatar = findViewById(R.id.avatar_tool);
        save = findViewById(R.id.fab_save);
        delete = findViewById(R.id.fab_delete);



        et_name.setText(fullname);
        et_phone.setText(phone);
        et_date.setText(date);
        et_address.setText(adress);
        avatar.setImageResource(ava);
        spinner = findViewById(R.id.spinner);
        if(group!=null) {
            for (int i = 0; i < spinner.getAdapter().getCount(); i++) {
                if (spinner.getAdapter().getItem(i) == group) {
                    spinner.setSelection(i);
                    break;
                }
            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent();
                back.putExtra("RES", id_contact);
                back.putExtra("ISDELETE", 0);
                back.putExtra("RES_NAME", et_name.getText().toString());
                back.putExtra("RES_PHONE", et_phone.getText().toString());
                back.putExtra("RES_ADRESS", et_address.getText().toString());
                back.putExtra("RES_DATE", et_date.getText().toString());
                back.putExtra("RES_GROUP",  spinner.getSelectedItem().toString());
                setResult(RESULT_OK, back);
                finish();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert = new AlertDialog.Builder(x);

                alert.setTitle("Delete");
                alert.setMessage("Are you sure?");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {


                        Intent back = new Intent();
                        back.putExtra("RES", id_contact);
                        back.putExtra("ISDELETE", 1);
                        setResult(RESULT_OK, back);
                        finish();
                    }
                });
                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                            }
                        });

                alert.show();

            }
        });



    }
}
