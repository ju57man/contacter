package com.ju57man.contacter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Messenger;
import android.telephony.SmsMessage;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;


public class SMSBReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent)
    {
        // Get Bundle object contained in the SMS intent passed in
        Bundle bundle = intent.getExtras();
        SmsMessage[] sms = null;
        String sms_str ="";
        if (bundle != null)
        {
            // Get the SMS message
            Object[] pdus = (Object[]) bundle.get("pdus");
            sms = new SmsMessage[pdus.length];
            for (int i=0; i<sms.length; i++){
                sms[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                sms_str += "Sent From: " + sms[i].getOriginatingAddress();
                sms_str += "\r\nMessage: ";
                sms_str += sms[i].getMessageBody();
                sms_str += "\r\n";
            }

            // Start Application's  MainActivty activity
            Intent smsIntent = new Intent(context,One_Contact.class);
            smsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            smsIntent.putExtra("sms_str", sms_str);
            context.startActivity(smsIntent);
        }
    }
}

